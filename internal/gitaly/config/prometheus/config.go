package prometheus

import (
	grpcprometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/gitaly/v14/internal/middleware/limithandler"
)

// DefaultBuckets returns the default gRPC latency bucket configuration for Prometheus.
func DefaultBuckets() []float64 {
	return []float64{0.001, 0.005, 0.025, 0.1, 0.5, 1.0, 10.0, 30.0, 60.0, 300.0, 1500.0}
}

// Config contains additional configuration data for prometheus
type Config struct {
	GRPCLatencyBuckets []float64 `toml:"grpc_latency_buckets"`
}

// Configure configures latency buckets for prometheus timing histograms
func (c *Config) Configure() {
	if len(c.GRPCLatencyBuckets) == 0 {
		return
	}

	log.WithField("latencies", c.GRPCLatencyBuckets).Info("grpc prometheus histograms enabled")

	grpcprometheus.EnableHandlingTimeHistogram(func(histogramOpts *prometheus.HistogramOpts) {
		histogramOpts.Buckets = c.GRPCLatencyBuckets
	})
	grpcprometheus.EnableClientHandlingTimeHistogram(func(histogramOpts *prometheus.HistogramOpts) {
		histogramOpts.Buckets = c.GRPCLatencyBuckets
	})

	limithandler.EnableAcquireTimeHistogram(c.GRPCLatencyBuckets)
}
